using UnityEngine;
public interface IInteractable {
    void interact();
}
public interface IDropable {
    void drop();
    void pickup();
}
public interface ITurn {
    void register();
    void activate();
    bool activated();
}
public interface ISelectable {
    GameObject select();
    void prompt(WorldObject executer);
}
public interface IMovable {
    int occType();
    void moveto(Coordinate target);
    void teleportto(Coordinate target);
}
