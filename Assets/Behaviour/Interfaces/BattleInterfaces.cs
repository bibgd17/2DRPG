using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IKillable {
    void destroy();
}
public interface IDamageble {
    void getDamage(int amount);
    void getMagicDamage(int amount);
    void getHeal(int amount);
}
public interface ICast {
    void Cast();
}
