using UnityEngine;
public class WorldObject : MonoBehaviour, ISelectable {
    public Coordinate position;

    public virtual void prompt(WorldObject executer)
    {
        throw new System.NotImplementedException();
    }

    public virtual GameObject select()
    {
        if(Turner.currentObject() is Player){
            Player player = Turner.currentObject() as Player;
            player.prompt(this);
        }
        return this.gameObject;
    }
}