using System.Collections.Generic;
using UnityEngine;
public static class Turner{
    public static List<Player> player = new List<Player>();
    
    static int TurnCount = 0;
    private static Dictionary<int, WorldObject> TurnOrder = new Dictionary<int, WorldObject>();
    public static void register(WorldObject registrand){
        ITurn iturn = registrand as ITurn;
        if(iturn != null){
            Debug.Log("registerted:  " + registrand.name);
            TurnOrder.Add(TurnOrder.Count, registrand);
        } else {
            Debug.Log("GameObject isnt an Object");
        }
    }
    public static void registerPlayer(Player newplayer){
        player.Add(newplayer);
    }
    public static WorldObject currentObject(){
        return TurnOrder[TurnCount];
    }
    public static void Next(){
        if(TurnOrder.Count - 1 == TurnCount){
            TurnCount = 0;
            Debug.Log("Reset");
        } else {
            TurnCount++;
        }
        CheckPlayer(TurnOrder[TurnCount]);
    }
    static void CheckPlayer(WorldObject obj){
        Player ply = obj as Player;
        if(ply != null){
            ply.Trigger();
        }
    }
    public static bool InRange(Coordinate CharacterPosition, Coordinate FieldPosition, int range,int jumprange){
        if(CalcRange(CharacterPosition.x, FieldPosition.x, range) && CalcRange(CharacterPosition.y, FieldPosition.y, jumprange) &&CalcRange(CharacterPosition.z, FieldPosition.z, range)){
            return true;
        } else {
            return false;
        }
    }
    static bool CalcRange(int xp, int xf, int r){
        if(xf <= xp + r && xf >= xp - r){
            return true;
        } else {
            return false;
        }
    }
    public static bool Selecting(){
        if(player.Contains(currentObject() as Player)) {
            return true;
        } else {
            return false;
        }
    }
}