using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : WorldObject
{
    public Renderer Frame;
    public Character occupant;
    public int thisMode;
    
    void Awake(){
        this.position = Coordination.CreateCoordinate(this);
        this.name = "Field#(" + position.x + ", " + position.y + ", " + position.z + ")";
        Frame = transform.Find("Frame").GetComponent<Renderer>();
    }

    public override GameObject select(){
        Character cChar = Turner.currentObject() as Player;
        if(cChar != null)if(cChar.fieldmode == 1 && Turner.InRange(cChar.position, this.position, cChar.Range, cChar.JumpRange) && cChar.fieldmode == thisMode){
            cChar.moveto(this.position);
         }
         return this.gameObject;
    }
    public void occupy(Character character) {
        Debug.Log("occupied" + this.name);
        occupant = (Character)character;
    }
    public void Leave(){
        occupant = null;
    }
    void Update(){
        if(occupant != null){
            if(occupant is Enemy){
                thisMode = 2;
            }
            if(occupant is Player){
                thisMode = 3;
            }
            
        } else {
            thisMode = 1;
        }
        
        Character cChar = Turner.currentObject() as Character;
        if(cChar.fieldmode == 1 && Turner.InRange(cChar.position, this.position, cChar.Range, cChar.JumpRange)){
            if(occupant == null){
                Frame.material.color = Color.yellow;
            } else {
                if(thisMode == 1){
                    Frame.material.color = Color.yellow;
                 }
                if(thisMode == 2){
                    Frame.material.color = Color.red;
                }
                if(thisMode == 3){
                    Frame.material.color = Color.green;
                }
            }
            
        } else {
            
            Frame.material = this.GetComponent<Renderer>().material;
        }
    }
    
}