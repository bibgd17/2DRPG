﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	// Use this for initialization
	Camera cam;
	public float sensitivity = 10F;
	void Start () {
		cam = transform.Find("Camera").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButton(1)){
			transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivity, 0);
		}
		this.transform.position = Turner.currentObject().transform.position;
	}
}	
