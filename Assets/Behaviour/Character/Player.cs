using UnityEngine;

public class Player : BattleCharacter {
    
    void Start(){
        occupytype = 1;
       register();
    }
    public void Trigger(){

    }
    void Update(){
        if(Turner.currentObject() == this){
            fieldmode = 1;
        } else {
            fieldmode = 0;
        }
    }
    public void prompt<T>(T Obj) where T : WorldObject {
        if(Obj is IDamageble){

        }
        if(Obj is IInteractable){
            IInteractable Target = Obj as IInteractable;
            Target.interact();
        }
    }
    
}