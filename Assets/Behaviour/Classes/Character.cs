using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Character : WorldObject, IMovable, ITurn{
    public int fieldmode = 0;
    public string Name;
    public Field myField;
    //Fieldmodulation
    public int occupytype;
    Vector3 modulator = new Vector3(0, 6, 0);
    public int Range;
    public int JumpRange;
    protected int MaxHealth;
    protected float Health;
    public Attribute attributes = new Attribute();
    public List<Ability> Abilitys;
    private float animationspeed = 0.05F;
    public void Cast(){

    }
    IEnumerator MoveAnimation() {
        
        Vector3 difference = new Vector3(0, 6, 0);
        for (float f = 1; transform.position != myField.transform.position; f++)
        {
            transform.position = Vector3.MoveTowards(transform.position, myField.transform.position + difference, 6*Time.deltaTime);
            
            yield return new WaitForSeconds(0.0001F);
        }
        moveani = null;
        yield return new WaitForSeconds(1);
        }
    protected void Awake(){
        MaxHealth = attributes.Strength + (attributes.Durability * 2);
        Health = MaxHealth;
        OccupyThisPosition();
    }
    protected void OccupyThisPosition(){
        this.myField = Coordination.GetField(Coordination.GetCoordinate(transform.position - modulator));
        Debug.Log(myField.name);
        myField.occupy(this);
    }
    

    public void register()
    {
        Turner.register(this);
    }

    public void activate()
    {
        throw new System.NotImplementedException();
    }

    public bool activated()
    {
        throw new System.NotImplementedException();
    }

    
    Coroutine moveani;
    public int occType(){
        return occupytype;
    }
    public void moveto(Coordinate target)
    {
        myField.Leave();   
        myField = Coordination.CoordDictionary[target];
        this.position = myField.position;
        myField.occupy(this);
        moveani = StartCoroutine("MoveAnimation");
    }

    public void teleportto(Coordinate target)
    {
        myField.Leave();
        Field targetField = Coordination.CoordDictionary[target];
        
        transform.position = targetField.transform.position + modulator;
        myField = targetField;
        myField.occupy(this);
    }

    
}