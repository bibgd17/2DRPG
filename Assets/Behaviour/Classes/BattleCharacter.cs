using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleCharacter : Character, IKillable, IDamageble{
    public void destroy()
    {
        Manager.destroy(this.gameObject);
    }

    public void getDamage(int dmg)
    {
        Health = Health - dmg;
    }
    public void getMagicDamage(int amount)
    {
        throw new System.NotImplementedException();
    }
    public void getHeal(int heal)
    {
        if(MaxHealth < Health + heal){
            Health = MaxHealth;
        } else {
            Health = Health + heal;
        }
    }
}