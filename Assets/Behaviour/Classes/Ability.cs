﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct Ability {
	public string name;
	public Texture icon;
	public int level;
	public int maxLevel;
	public int targets;
	public float psScale;
	public float ssScale;
	bool heal;
	public void execute(BattleCharacter[] Targets, int primestat, int secstat){
		foreach(BattleCharacter Target in Targets){
			if(heal){
				Target.getHeal((int)Mathf.Round(primestat * (psScale * level) + secstat * (ssScale * level)));
			} else {
				Target.getDamage((int)Mathf.Round(primestat * (psScale * level) + secstat * (ssScale * level)));
			}
		}
	}
	public Ability(string nm, Texture IC, int lvl,int maxlvl, int trg, float psS, float ssS, bool hl){
		name = nm;
		icon = IC;
		targets = trg;
		level = lvl;
		maxLevel = maxlvl;
		psScale = psS;
		ssScale = ssS;
		heal = hl;
	}
}