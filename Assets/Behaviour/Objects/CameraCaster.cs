using UnityEngine;
public class CameraCaster : MonoBehaviour {
    private Camera cam;
    void Awake(){
        cam = GetComponent<Camera>();
    }
void Update(){
   if (Input.GetMouseButtonDown(0)){ // if left button pressed...
     Ray ray = cam.ScreenPointToRay(Input.mousePosition);
     RaycastHit hit;
     if (Physics.Raycast(ray, out hit)){
         Debug.Log("Clicked on: " + hit.transform.name);
        hit.transform.GetComponent<WorldObject>().select();
     }
   }
 }
}