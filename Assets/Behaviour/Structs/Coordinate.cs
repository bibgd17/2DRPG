using UnityEngine;
using System.Collections.Generic;
public struct Coordinate {
    public readonly int x;
    public readonly int y;
    public readonly int z;
    public Coordinate(float cx, float cy, float cz){
        x=(int)cx;
        y=(int)cy;
        z=(int)cz;
    }
}
public static class Coordination {
    public static int XModifier = 12;
    public static int YModifier = 1;
    public static int ZModifier = 12;
    public static Dictionary<Coordinate, Field> CoordDictionary = new Dictionary<Coordinate, Field>();
     public static Coordinate CreateCoordinate(Field field){
         Coordinate coord = new Coordinate(field.transform.position.x / XModifier,field.transform.position.y / YModifier, field.transform.position.z / ZModifier);
         CoordDictionary.Add(coord, field);
         return coord;
     }
     public static Coordinate GetCoordinate(Vector3 position){
         return new Coordinate(position.x / XModifier,position.y / YModifier, position.z / ZModifier);
     }
     public static Field GetField(Coordinate coord){
         if(CoordDictionary.ContainsKey(coord)){
             return CoordDictionary[coord];
         } else {
             return null;
         }
         
     }
}