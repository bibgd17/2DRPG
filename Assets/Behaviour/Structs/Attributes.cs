public struct Attribute {
    public int Strength;
    public int Durability;
    public int Agility;
    public int Dexterity;
    public int Intelligence;
    public int Wisdom;

    public Attribute(int strength, int durability, int agility, int dexterity, int intelligence, int wisdom) {
        Strength = strength;
        Durability = durability;
        Agility = agility;
        Dexterity = dexterity;
        Intelligence = intelligence;
        Wisdom = wisdom;
    }
}
