using UnityEngine;
using System.Collections;
public class Item {
    public string name;
    public string description;
    public int id;
}
public class Weapon : Item  {
    public  int Damage;
    public  int AttributesType;
    public  int MinimumAttribute;
    public  Texture WeaponIcon;
}

public struct WeaponBlueprint {
    public readonly string name;
    public readonly string description;
    public readonly int id;
    public readonly int Damageminimum;
    public readonly int Damagemaximum;
    public readonly int AttributesType;
    public readonly int MinimumAttribute;
    public readonly Texture WeaponIcon;

    public WeaponBlueprint(string nm, string desc, int weaponid, int dmgmin, int dmgmax, int attrtype, int minattr, Texture img){
        name = nm;
        description = desc;
        id = weaponid;
        Damagemaximum = dmgmax;
        Damageminimum = dmgmin;
        AttributesType = attrtype;
        MinimumAttribute = minattr;
        WeaponIcon = img;
    }
}