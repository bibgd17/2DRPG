using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AbilityListWindow : EditorWindow {
    [MenuItem("Window/Ability Lists")]
    public static void ShowWindow(){
        GetWindow<AbilityListWindow>("Ability Lists");
    }
}