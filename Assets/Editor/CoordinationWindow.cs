using UnityEditor;
using UnityEngine;

public class CoordinationWindow : EditorWindow {

    [MenuItem("Window/Coordination")]
    public static void ShowWindow(){
        GetWindow<CoordinationWindow>("Coordination");
    }

    void OnGUI(){
        EditorGUILayout.LabelField("Coordination System Scaling", EditorStyles.largeLabel);
        Coordination.XModifier = EditorGUILayout.IntField("X Scale", Coordination.XModifier);
        Coordination.YModifier = EditorGUILayout.IntField("Y Scale", Coordination.YModifier);
        Coordination.ZModifier = EditorGUILayout.IntField("Z Scale", Coordination.ZModifier);
    }
}