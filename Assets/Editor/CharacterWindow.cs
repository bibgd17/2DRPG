using UnityEditor;
using UnityEngine;

public class CharacterWindow : EditorWindow {
    Character target;
    int Strength;
    int Agility;
    int Intelligence;
    int Durability;
    int Dexterity;
    int Wisdom;
    [MenuItem("Window/CharacterEditor")]
    public static void ShowWindow(){
        GetWindow<CharacterWindow>("Character");
    }
    void OnGUI(){
        GUILayout.Label("CharacterEditor", EditorStyles.boldLabel);
        target = Selection.activeGameObject.GetComponent<Character>();
        if(target !=null){
            
            target.attributes.Strength = EditorGUILayout.IntField("Strength:", target.attributes.Strength);
            target.attributes.Agility = EditorGUILayout.IntField("Agility:", target.attributes.Agility);
            target.attributes.Intelligence = EditorGUILayout.IntField("Intelligence:", target.attributes.Intelligence);
            target.attributes.Durability = EditorGUILayout.IntField("Durability:", target.attributes.Durability);
            target.attributes.Dexterity = EditorGUILayout.IntField("Dexterity:", target.attributes.Dexterity);
            target.attributes.Wisdom = EditorGUILayout.IntField("Wisdom:", target.attributes.Wisdom);
        }
    }
    
}