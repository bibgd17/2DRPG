using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Field))]
public class FieldEditor : Editor {
    public override void OnInspectorGUI(){
        Field field = (Field) target;
        base.OnInspectorGUI();
        if(GUILayout.Button("Fix position")){
            int x;
            int y;
            int z;
            x = (int)Mathf.Round(field.transform.position.x / Coordination.XModifier);
            y = (int)Mathf.Round(field.transform.position.y / Coordination.YModifier);
            z = (int)Mathf.Round(field.transform.position.z / Coordination.ZModifier);
            field.transform.position = new Vector3(x * Coordination.XModifier, y * Coordination.YModifier, z * Coordination.ZModifier);
        }
    }
}