using UnityEditor;
using UnityEngine;

public class AbilityWindow : EditorWindow {
    public Character target;
    public string name;
	public Object icon;
	public int targets;
    public int maxlevel;
    public float psadd;
    public float ssadd;
    public bool heal;
    [MenuItem("Window/AbilityCreator")]
    public static void ShowWindow(){
        GetWindow<AbilityWindow>("Ability Creator");
    }
    void OnGUI(){
        target = Selection.activeGameObject.GetComponent<Character>();
        name = EditorGUILayout.TextField("Name", name);
        EditorGUILayout.LabelField("Icon Texture");
        icon = EditorGUILayout.ObjectField(icon, typeof(Texture), true);
        targets = EditorGUILayout.IntField("Target amount", targets);
        maxlevel = EditorGUILayout.IntField("Max Level", maxlevel);
        psadd = EditorGUILayout.FloatField("Primary Scale add per lvl", psadd);
        ssadd = EditorGUILayout.FloatField("Secondary Scale add per lvl", ssadd);
        EditorGUILayout.LabelField("Is it a heal?");
        heal = EditorGUILayout.Toggle(heal);
        if(GUILayout.Button("Create")){
            Ability ability = new Ability(name, (Texture)icon, 0, maxlevel, targets, psadd, ssadd, heal);
            target.Abilitys.Add(ability);
        }
    }
    
}